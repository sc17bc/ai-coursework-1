from __future__ import print_function
from copy import deepcopy
from tree import *
from queue_search import *

#Representation of a state:
#An mxn 2d array, where 3 corresponds to a new queen, 2 corresponds to an existing queen, 1 is a covered space, and 0 is an uncovered space

def queen_initalise(): #initial state
    return [ [0 for i in range(x)] for j in range(y)]

def info_function():
    print ("A minimal arrangement of queens to cover all the spaces on a ", x, "x", y, " board")

def possible_actions(state):
    actions = []
    for i in range(y):
        for j in range(x):
            if state[i][j] == 0:
                actions = actions + [[i, j]]
    return actions

def successor_function(action, state):
    a = action[0]
    b = action[1]
    board = deepcopy(state)
    board[a][b] = 2
    for i in range(x):
        if board[a][i] != 2:
            board[a][i] = 1
    for i in range(y):
        if board[i][b] != 2:
            board[i][b] = 1
    for i in range(x):
        if a + i < y and a - i > -1:
            if board[a + i][i] != 2:
                board[a + i][i] = 1
            if board[a - i][i] != 2:
                board[a - i][i] = 1
    return board

def goal_test(state):
    board = deepcopy(state)
    for i in range(y):
        for j in range(x):
            if board[i][j] == 0:
                return False
    print_board_state(board)
    return True

def print_board_state( state ):
      board = deepcopy(state)
      print("\n")
      for row in board:
           for square in row:
               print( " %2i" % square, end = '' )
           print()

def make_qc_problem(m,n):
    global x, y
    x = m
    y = n
    return(None, info_function, queen_initalise(), possible_actions, successor_function, goal_test)
